/**
 * Main application file
 */

const express = require("express");
const compression = require("compression");
import http from "http";
import https from "https";
import fs from "fs";
const pathApp = require("path");
const publicDir = pathApp.join(__dirname, "/public");
const config = require("./src/config/environment");
const logger = require("./src/config/logger");
const expressConfig = require("./src/config/express");
const route = require("./routes");
const { port } = require("./src/config/environment/production");
const app = express();

logger.info(
  `****************************** ENTORNO DE TRABAJO [ ${process.env.NODE_ENV}, use port ${config.port} ] ******************************`
);

app.use(express.static(publicDir));
app.use(compression());

let server: any;
if (config.port === port) {
  server = https.createServer(
    {
      key: fs.readFileSync(pathApp.join(__dirname + "/src", "cert", "key.pem")),
      cert: fs.readFileSync(
        pathApp.join(__dirname + "/src", "cert", "cert.pem")
      ),
    },
    app
  );
} else server = http.createServer(app);

expressConfig(app);
route(app);

// Start server
function startServer() {
  app.angularFullstack = server.listen(config.port, config.ip, () => {
    logger.info(
      `Express server listening on ${config.port}, in ${app.get("env")} mode`
    );
  });
}

setImmediate(startServer);

export default app;
