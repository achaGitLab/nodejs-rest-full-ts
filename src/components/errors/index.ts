/**
 * Error responses
 * @author: Ronald Acha Ramos
 */

 import { Request, Response } from "express";

module.exports[404] = function pageNotFound(req: Request, res: Response) {
  const viewFilePath = '404';
  const statusCode = 404;
  const result = {
    status: statusCode,
  };

  res.status(result.status);
  res.render(viewFilePath, {}, (err, html) => {
    if (err) {
      return res.status(result.status).json(result);
    }

    return res.send(html);
  });
};
