'use strict';
const { v4: uuidv4 } = require('uuid');
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('UserData', {
      id: {
        allowNull: false,
        // autoIncrement: true,
        primaryKey: true,
        type: Sequelize.STRING(50),
        defaultValue: uuidv4(),
      },
      idUser: {
        type: Sequelize.STRING(50),
        references: {
          model: 'Users',
          key: 'id',
        },
        onUpdate: 'RESTRICT',
        onDelete: 'RESTRICT',
      },
      name: {
        type: Sequelize.STRING(200),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: '"El valor \'name\' es obligatorio."',
          },
        },
      },
      lastName: {
        type: Sequelize.STRING(200),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: '"El valor \'last_name\' es obligatorio."',
          },
        },
      },
      documentIdentity: {
        type: Sequelize.STRING(50),
        validate: {
          notEmpty: {
            msg: '"El valor \'document_identity\' es obligatorio."',
          },
        },
      },
      celular: {
        type: Sequelize.STRING(50),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: '"El valor \'celular\' es obligatorio."',
          },
        },
      },
      birthday: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: Sequelize.NOW,
      },
      termConditions: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      receiveNotification: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      deleteAt: {
        type: Sequelize.DATE,
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('UserData');
  },
};
