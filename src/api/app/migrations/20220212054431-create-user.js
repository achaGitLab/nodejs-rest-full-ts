'use strict';
const { v4: uuidv4 } = require('uuid');
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING(50),
        defaultValue: uuidv4(),
      },
      token: {
        type: Sequelize.STRING(32),
        allowNull: false,
        unique: true,
        validate: {
          notEmpty: {
            msg: '"El valor \'token\' es obligatorio."',
          },
        },
      },
      username: {
        type: Sequelize.STRING(100),
        allowNull: false,
        unique: true,
        validate: {
          notEmpty: {
            msg: '"El valor \'username\' es obligatorio."',
          },
        },
      },
      password: {
        type: Sequelize.STRING(255),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: '"El valor \'password\' es obligatorio."',
          },
        },
      },
      salt: {
        type: Sequelize.STRING(50),
        allowNull: false,
        unique: true,
        validate: {
          notEmpty: {
            msg: '"El valor \'salt\' es obligatorio."',
          },
        },
      },
      backgroundColor: {
        type: Sequelize.STRING(50),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: '"El valor \'background_color\' es obligatorio."',
          },
        },
      },
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      deleteAt: {
        type: Sequelize.DATE,
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Users');
  },
  beforeSave: (user, options) => {
    if (user.changed('password')) {
      const scriptHash = encryptPassword(user.password);
      user.password = scriptHash.passwordHash;
      user.salt = scriptHash.salt;
    }
  },
};
