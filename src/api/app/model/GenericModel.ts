import { IGenericModel } from "../interfaces/IGenericModel";
const Logger = require("./../../../config/logger");
const { getErrorMessage, saltHashPassword } = require("../../../config/utils");
import { Response } from "express";
const https = require("https");

export class GenericModel implements IGenericModel {
  protected repository: any;
  protected codeFunction: string = "";
  protected codeError: string = "";

  constructor(reposotiry: any) {
    this.repository = reposotiry;
  }

  /**
   *
   * @param user
   * @codeFuncion S
   * @returns
   */
  public async save(object: any) {
    try {
      const save = await this.repository.save(object);
      return save;
    } catch (e: any) {
      this.error500(e, this.codeError, "S");
    }
  }

  /**
   * update user
   * @param {*} user
   * @codeFuncion U
   */
  public async update(object: any) {
    try {
      if (object.hasOwnProperty("id")) {
        const updated = await this.repository.update(object);
        return !!updated[0];
      }
      return false;
    } catch (e: any) {
      this.error500(e, this.codeError, "U");
    }
    return false;
  }

  /**
   *
   * @param id
   * @codeFuncion FBI
   * @returns
   */
  public async findById(id: string) {
    try {
      return await this.repository.findById(id);
    } catch (e: any) {
      this.error500(e, this.codeError, "FBI");
    }
  }

  /**
   *
   * @param id
   * @codeFuncion D
   * @returns
   */
  public async delete(id: string) {
    try {
      return this.repository.destroy({
        where: { id: id },
      });
    } catch (e: any) {
      this.error500(e, this.codeError, "D");
    }
  }

  public error500(e: any, codeError: string, codeFunction: string) {
    const message = `${codeError}-${codeFunction}`;
    e.codeMessage = message;
    Logger.error(`${message}-${e.message}`);
    Logger.error(e);
    throw new Error(`${message}-${getErrorMessage(e)}`);
  }

  public exceptionBasic(msg: string) {
    Logger.error(`${msg}`);
    throw new Error(`${msg}`);
  }
}
