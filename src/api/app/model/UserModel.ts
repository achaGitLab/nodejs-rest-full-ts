import { UserRepository } from "../Repository";
const Logger = require("./../../../config/logger"); //src/config/logger
import { GenericModel } from "./GenericModel";
const { saltHashPassword } = require("../../../config/utils");

export class UserModel extends GenericModel {
  private CODE_BASE_ERROR = "MUEX"; // MODELO SHEET EXCEPTION

  constructor() {
    super(UserRepository);
    super.codeError = this.CODE_BASE_ERROR;
  }

  /**
   *
   * @param username
   * @codeFuncion FA
   */
  public findAll = async () => {
    try {
      return await this.repository.findAll();
    } catch (e: any) {
      this.error500(e, this.codeError, "FA");
    }
  }

  /**
   *
   * @param username
   * @codeFuncion FBU
   */
  public findByUsername = async (username: string) => {
    try {
      return await UserRepository.findByUsername(username);
    } catch (e: any) {
      return this.error500(e, this.CODE_BASE_ERROR, "FBU");
    }
  };
}
