import { UserModel } from "./UserModel";
import { UserDataModel } from "./UserDataModel";

const userModel = new UserModel();
const userDataModel = new UserDataModel();

export {
  userModel as UserModel,
  userDataModel as UserDataModel,
}
