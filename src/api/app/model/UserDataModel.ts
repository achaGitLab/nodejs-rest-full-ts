import { UserDataRepository } from "../Repository/";
import { GenericModel } from "./GenericModel";
const Logger = require("./../../../config/logger");

export class UserDataModel extends GenericModel {
  private CODE_BASE_ERROR = "MUDMEX";
  constructor() {
    super(UserDataRepository);
    super.codeError = this.CODE_BASE_ERROR;
  }
}
