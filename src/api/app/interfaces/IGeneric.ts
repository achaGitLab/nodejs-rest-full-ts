export interface IGeneric {
  save <T extends Object> (userEntity: any) : Promise<T>;
  findById<T extends Object>(id: string): Promise<T>;
  update (userEntity: any): Promise<boolean>;
  delete <T extends Object> (userEntity: any): Promise<T>;
}