export interface IGenericModel {
  save<T extends Object>(userEntity: any): Promise<T>;
  findById<T extends Object>(id: string): Promise<T>;
  update(userEntity: any): Promise<boolean>;
  delete<T extends Object>(userEntity: any): Promise<T>;
  error500<T extends Object>(e: any, codeError: string, codeFuncion: string): void;
}
