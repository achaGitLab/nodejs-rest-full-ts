import { GenericRepository } from "./GenericRepository";
import config from "../models/index";
const { DataTypes } = require("sequelize");
const UserData = require("../models/UserData")(config.sequelize, DataTypes);

export interface IUserDataRepo {
  getUserData<T extends Object>(): T;
  findByEmail <T extends Object> (email: string) : Promise<T>;
  findByIdUser<T extends Object>(idUser: string): Promise<T>;
}

export class UserDataRepository
  extends GenericRepository
  implements IUserDataRepo
{
  constructor() {
    super(UserData);
  }

  public getUserData = () => {
    return UserData;
  }

  /**
   * get user by email
   * @param {*} email
   */
  public findByEmail = async (email: string) => {
    const userData = await UserData.findOne({
      where: {
        email,
      },
    });
    return userData || undefined;
  };
  /**
   * get userData by idUser
   * @param {*} idUser
   */
  public findByIdUser = async (idUser: string) => {
    if (idUser) {
      const userData = await UserData.findOne({
        where: { idUser },
      });
      return userData || undefined;
    }
    return undefined;
  };
}
