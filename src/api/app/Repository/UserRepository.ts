import { GenericRepository } from "./GenericRepository";
import config from "./../models/index";
const { DataTypes } = require("sequelize");
const User = require("../models/User")(config.sequelize, DataTypes);
//const Token = require("../models/Token")(config.sequelize, DataTypes);

//ejemplo
//https://github.com/antonrodin/sequelize/blob/master/cli-models/config/database.js

export interface IUserRepo {
  getUser<T extends Object>(): T;
  findAll<T extends Object>(): Promise<T>;
  findByUsername<T extends Object>(username: string): Promise<T>;

}

export class UserRepository extends GenericRepository implements IUserRepo {
  constructor() {
    super(User);
  }

  public getUser = () => {
    return User;
  };

  /**
   *
   * @param username
   * @codeFuncion FA
   */
  public findAll = async () => {
    return await User.findAll();
  };

  /**
   * get user by username
   * @param {*} username
   */
  public findByUsername = async (username: string) => {
    const result = await User.findOne({ where: { username } });
    return result;
  };
}
