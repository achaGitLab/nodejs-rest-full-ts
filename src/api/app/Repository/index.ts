import { UserRepository } from "./UserRepository";
import { UserDataRepository } from "./UserDataRepository";

const userRepository = new UserRepository();
const userDataRepository = new UserDataRepository();

//  User  and  userData
userRepository.getUser().hasMany(userDataRepository.getUserData(), {
  foreignKey: "idUser",
});
userDataRepository.getUserData().belongsTo(userRepository.getUser(), {
  foreignKey: "idUser",
  targetKey: "id",
});

export {
  userRepository as UserRepository,
  userDataRepository as UserDataRepository,
};
