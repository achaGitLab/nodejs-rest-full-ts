import {IGeneric} from "./../interfaces/IGeneric";

export class GenericRepository implements IGeneric {
  protected entityDb: any;
  constructor(entityP: any) {
    this.entityDb = entityP;
  }

  public get (){
    return this.entityDb;
  }

  public set (entityP: any){
    return this.entityDb = entityP;
  }

  public async save(object: any) {
    const saveEntity = await this.entityDb.create(object);
    return saveEntity;
  }

  public async update(object: any) {
    const updateEntity = await this.entityDb.update(object, { where: { id: object.id } });
    return updateEntity;
  }

  public async findById(id: string){
    const result = await this.entityDb.findOne({
      where: { id: id },
    });
    return result;
  }        

  public async delete(id: string) {
    return this.entityDb.destroy({ 
      where: { id: id }
    });
  }
}
