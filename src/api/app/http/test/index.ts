/**
 * User routes
 * @author: Ronald Acha Ramos
 */
 const { Router } = require('express');
 import { TestController } from "./TestController";
 
 const router = new Router();
 const controller = new TestController();

//router.post('/', controller.create);
router.get('/', controller.hola);

export const test = router;