import { EstadosHttp } from "./EstadosHttp";

const estadosHttp = new EstadosHttp();

const response403 = estadosHttp.response403;
const response500 = estadosHttp.response500;
const response204 = estadosHttp.response204;
const response200 = estadosHttp.response200;

export { response403, response500, response204, response200 };
