/**
 * @author: Ronald Acha
 */
import { Response } from "express";
const Logger = require("../../../../config/logger");
const { getErrorMessage } = require("../../../../config/utils");

class EstadosHttp {
  public response500 = (
    e: any,
    data: any = undefined,
    res: Response,
    msg: string
  ) => {
    e.codeMessage = msg;
    Logger.error(`${msg}-${e.message}`);
    Logger.error(e);
    return res.status(500).json({
      result: false,
      data: data ? data : {},
      message: `${msg}-${getErrorMessage(e)}`,
    });
  };

  public response403 = (
    data: any = undefined,
    res: Response,
    msg: string
  ) => {
    Logger.error(`${msg}`);
    return res.status(403).json({
      result: false,
      data: data ? data : {},
      message: `${msg}`,
    });
  };

  public response204 = (
    data: any = undefined,
    res: Response,
    msg: string
  ) => {
    Logger.error(`${msg}`);
    return res.status(204).json({
      result: false,
      data: data ? data : {},
      message: `${msg}`,
    });
  };

  public response200 = (
    data: any = undefined,
    res: Response,
    msg: string
  ) => {
    Logger.error(`${msg}`);
    return res.status(200).json({
      result: false,
      data: data ? { data } : {},
      message: `${msg}`,
    });
  };
}

export { EstadosHttp };
