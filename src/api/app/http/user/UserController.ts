/**
 * Using Rails-like standard naming convention for endpoints.
 * @author: Ronald Acha Ramos
 */
const { UserModel, UserDataModel } = require("./../../model");
import { Request, Response } from "express";
const { response200, response403, response500 } = require("./../estadosHttp");

const CODE_BASE_ERROR = "UCEX"; //Oauth Controller EXception

class UserController {
  /**
   * register new user
   * @param {*} req
   * @param {*} res
   * @codeFunction S
   */
  public users = async (req: Request, res: Response) => {
    try {
      const usersDb = await UserModel.findAll();
      const result = {
        result: true,
        data: { user: usersDb },
        message: `${"Acción realizada con exito"}`,
      };
      return res.status(200).json(result);
    } catch (e: any) {
      return response500(
        e,
        { user: {}, userData: {} },
        res,
        `${CODE_BASE_ERROR}-S`
      );
    }
  };

  /**
   * register new user
   * @param {*} req
   * @param {*} res
   * @codeFunction S
   */
  public save = async (req: Request, res: Response) => {
    const { user, userData } = req.body;
    try {
      user.backgroundColor = "blue";
      const userSave = await UserModel.save(user);
      if (userSave) {
        userData.idUser = userSave.dataValues.id;
        const userDataDb = await UserDataModel.save(userData);

        delete userSave.dataValues.password;
        delete userSave.dataValues.salt;

        // insertamos en la tabla confirm_user
        const confirmUserInput = {
          idUser: userSave.dataValues.id,
        };
        const result = {
          result: true,
          data: { user: userSave, userData: userDataDb },
          message: `${"Acción realizada con exito"}`,
        };
        return res.status(200).json(result);
      }

      return response403(
        { user: {}, userData: {} },
        res,
        `${"Acción no completada"}, ${"Usuario no registrado"}`
      );
    } catch (e: any) {
      console.log("++++++++++++++++ ", e);
      return response500(
        e,
        { user: {}, userData: {} },
        res,
        `${CODE_BASE_ERROR}-S`
      );
    }
  };
}
export { UserController };
