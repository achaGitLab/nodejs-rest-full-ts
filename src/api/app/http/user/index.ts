/**
 * User routes
 * @author: Ronald Acha Ramos
 */

const { Router } = require("express");
import { UserController } from "./UserController";

const router = new Router();
const controller = new UserController();

router.post(
  "/",
  controller.save
);
router.get(
  '/',
  controller.users,
);


export const user = router;
