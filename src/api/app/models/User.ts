"use strict";
const { v4: uuidv4 } = require('uuid');
import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from "sequelize";
const { encryptPassword, genRandomString } = require("../../../config/utils");

interface UserAttributes {
  token: string;
  username: string;
  password: string;
  salt: string;
  backgroundColor: string;
}

module.exports = (sequelize: any, DataTypes: any) => {
  class User extends Model<UserAttributes> implements UserAttributes {
    token!: string;
    username!: string;
    password!: string;
    salt!: string;
    backgroundColor!: string;
    static associate(models: any) {
      // define association here
    }
  }
  User.init(
    {
      token: {
        type: DataTypes.STRING(32),
        allowNull: false,
        unique: true,
        validate: {
          notEmpty: {
            msg: "\"El valor 'token' es obligatorio.\"",
          },
        },
        defaultValue: genRandomString(32),
      },
      username: {
        type: DataTypes.STRING(100),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: "\"El valor 'username' es obligatorio.\"",
          },
          notNull: {
            msg: "\"El valor 'username' debe estar definido.\"",
          },
        },
      },
      password: {
        type: DataTypes.STRING(255),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: "\"El valor 'password' es obligatorio.\"",
          },
          notNull: {
            msg: "\"El valor 'password' debe estar definido.\"",
          },
        },
      },
      salt: {
        type: DataTypes.STRING(50),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: "\"El valor 'salt' es obligatorio.\"",
          },
          notNull: {
            msg: "\"El valor 'salt' debe estar definido.\"",
          },
        },
      },
      backgroundColor: {
        type: DataTypes.STRING(50),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: "\"El valor 'background_color' es obligatorio.\"",
          },
          notNull: {
            msg: "\"El valor 'background_color' debe estar definido.\"",
          },
        },
      },
      active: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
    {
      hooks: {
        beforeValidate: (user: any, options) => {
          console.log("beforeValidate ************************** ", user);
          if (user.changed("password") && !user.id) {
            //console.log("--------------");
            const scriptHash = encryptPassword(user.password);
            user.password = scriptHash.passwordHash;
            user.salt = scriptHash.salt;
          }
        },
        /*afterValidate: (user, options) => {
        },
        beforeCreate: (user, options) => {
        },*/
      },
      sequelize,
      modelName: "User",
    }
  );

  /*User.beforeCreate((user) => {
    //if (user.changed('password')) {
    const scriptHash = encryptPassword(user.password);
    user.password = scriptHash.passwordHash;
    user.salt = scriptHash.salt;
    //}
  });*/

  /*User.beforeSave((user) => {
    if (user.changed("password")) {
      const scriptHash = encryptPassword(user.password);
      user.password = scriptHash.passwordHash;
      user.salt = scriptHash.salt;
    }
  });*/
  User.beforeSave((user: any) => {
    user.id = uuidv4();
    user.token = genRandomString(32); 
  });
  User.beforeUpdate((user: any) => {
    //user.updatedAt = DataTypes.DATE
    //user.token = genRandomString(32); 
    user.id = uuidv4();
  });
  return User;
};
