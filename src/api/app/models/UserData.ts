"use strict";
import { Model } from "sequelize";
const { v4: uuidv4 } = require('uuid');

interface UserDataAttributes {
  //id: string,
  idUser: string;
  name: string;
  lastName: string;
  documentIdentity: string;
  celular: string;
  birthday: Date;
  termConditions: boolean;
  receiveNotification: boolean;
  active:boolean;
}

module.exports = (sequelize: any, DataTypes: any) => {
  class UserData
    extends Model<UserDataAttributes>
    implements UserDataAttributes
  {
    //id!: string;
    idUser!: string;
    name!: string;
    lastName!: string;
    documentIdentity!: string;
    celular!: string;
    birthday!: Date;
    termConditions!: boolean;
    receiveNotification!: boolean;
    active!:boolean;
    static associate = (models: any) => {
    };
  }
  UserData.init(
    {
      /*id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.STRING(50),
        defaultValue: sequelize.fn('makeuid'),
      },*/
      idUser: {
        type: DataTypes.STRING(50),
        references: {
          model: 'User',
          key: 'id',
        },
        onUpdate: 'RESTRICT',
        onDelete: 'RESTRICT',
      },
      name: {
        type: DataTypes.STRING(200),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: '"El valor \'Name\' es obligatorio."',
          },
          notNull: {
            msg: "\"El valor 'Name' debe estar definido.\"",
          },
        },
      },
      lastName: {
        type: DataTypes.STRING(200),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: '"El valor \'Apellidos\' es obligatorio."',
          },
          notNull: {
            msg: "\"El valor 'Apellidos' debe estar definido.\"",
          },
        },
      },
      documentIdentity: {
        type: DataTypes.STRING(50),
        validate: {
          notEmpty: {
            msg: '"El valor \'document_identity\' es obligatorio."',
          },
        },
      },
      celular: {
        type: DataTypes.STRING(50),
        allowNull: false,
        validate: {
          notEmpty: {
            msg: '"El valor \'Celular\' es obligatorio."',
          },
          notNull: {
            msg: "\"El valor 'Celular' debe estar definido.\"",
          },
        },
      },
      birthday: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: DataTypes.NOW,
      },
      termConditions: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      receiveNotification: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      active: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
    },
    {
      sequelize,
      modelName: "UserData",
    }
  );
  UserData.beforeSave((userData: any) => {
    userData.id = uuidv4();
  });
  UserData.beforeUpdate((userData: any) => {
    userData.updatedAt = DataTypes.DATE;
  });
  //UserData.afterUpdate(instance, options)
  //UserData.afterSave(instance, options)
  return UserData;
};
