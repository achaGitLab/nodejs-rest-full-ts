/**
 * Express configuration
 * @author: Ronald Acha Ramos
 */

import { Application } from "express";
import express from 'express';
const compression = require('compression');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');
const pathExpress = require('path');
const cors = require('cors');
const config = require('./environment');

module.exports = (app: Application) => {
  const env = app.get('env');

  if (env === 'development' || env === 'test') {
    app.use(express.static(pathExpress.join(config.root, '.tmp')));
  }
  app.use(cors());
  app.set('appPath', pathExpress.join(config.root, ''));
  app.use(express.static(app.get('appPath')));

  //app.set('views', `${config.root}/server/views`);
  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(methodOverride());
  app.use(cookieParser());
};
