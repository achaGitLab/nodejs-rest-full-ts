/**
 * Development specific configuration
 * @author: Ronald Acha Ramos
 */

module.exports = {
  // Seed database on startup
  seedDB: false,
};
