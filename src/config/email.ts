/**
 * Express configuration
 * @author: Ronald Acha Ramos
 */
const nodemailer = require('nodemailer');
const winston = require('winston');
const hbs = require('nodemailer-express-handlebars');
const config = require('./src/environment/index');

const transporter = nodemailer.createTransport({
  host: config.stmp.host,
  port: config.stmp.port,
  secure: true,
  auth: {
    user: config.stmp.user,
    pass: config.stmp.password,
  },
  ignoreTLS: true,
  secureConnection: true,
  requiresAuth: true,
  domains: config.stmp.domains,
  connectionTimeout: config.stmp.connectionTimeout,
  greetingTimeout: config.stmp.greetingTimeout,
  debug: true,
});

const options = {
  viewEngine: {
    partialsDir: `${config.root}/erp-api/views/partials`,
    layoutsDir: `${config.root}/erp-api/views/layouts`,
    extname: '.hbs',
  },
  extName: '.hbs',
  viewPath: 'views/email',
};

winston.info(
  `Root de compilación para los email ${config.root}/erp-api/views/email`,
);
transporter.use('compile', hbs(options));

/**
 * permit verify the connection to smtp
 */
const verifyConnection = async () => {
  try {
    const verify = await transporter.verify();
    return verify;
  } catch (error) {
    return error;
  }
};

/**
 * permit send mail
 * @param {*} mailOptions
 */
const sendMail = async (mailOptions: any) => {
  try {
    const optionsSM = {
      from: config.stmp.user,
      ...mailOptions,
    };
    const sended = await transporter.sendMail(optionsSM);
    return sended;
  } catch (error) {
    return error;
  }
};

module.exports = {
  verifyConnection,
  sendMail,
};
