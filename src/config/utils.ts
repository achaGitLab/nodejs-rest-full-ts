/**
 * line model
 * @author: Ronald Acha Ramos
 */
import { Response } from "express";
import { Json } from "sequelize/types/utils";
const { randomBytes, createHmac } = require("crypto");
const fs = require("fs");

const arrayArticle = [
  "de",
  "y",
  "el",
  "los",
  "las",
  "la",
  "lo",
  "un",
  "una",
  "unos",
  "unas",
];

/**
 * get string in format hexadecimal random
 * @param {*} length lenght of text
 */
const genRandomString = function (length: number) {
  return randomBytes(Math.ceil(length / 2))
    .toString("hex") /** convert to hexadecimal format */
    .slice(0, length); /** return required number of characters */
};

/**
 * encrita el password con su respectivo salt
 * @param {*} password
 * @param {*} salt
 * @private
 */
const sha512 = (password: string, salt: string) => {
  const hash = createHmac("sha512", salt);
  hash.update(password);
  const value = hash.digest("hex");
  return {
    salt,
    passwordHash: value,
  };
};

/**
 * Encrypt password
 * @param {String} password
 * @param {Function} callback
 * @return {String}
 * @api public
 */
const encryptPassword = (password: string) => {
  const salt = genRandomString(16);
  return sha512(password, salt);
};

/**
 * metodo que permite encriptar un password mediante el salt generado anteriormente
 * @param {*} userpassword
 * @param {*} salt
 * @private
 */
const saltHashPassword = (userpassword: string, salt: string) => {
  const passwordData = sha512(userpassword, salt); // "2b3c5446b291c134"
  return passwordData.passwordHash;
};

/**
 * Encrypt password
 * @param {String} password
 * @param {Function} callback
 * @return {String}
 * @api public
 */
const getAttributesOfModel = (Model: any) => {
  const atributes = Object.entries(Model.rawAttributes).map((key) => {
    const atribute: any = key[1];
    const keyData = key[0].toString();
    if (keyData === "id") return undefined;
    if (keyData === "createdAt") return undefined;
    if (keyData === "updatedAt") return undefined;
    if (keyData === "deletedAt") return undefined;
    return keyData;
  });

  return atributes.filter((attribute) => attribute);
};

/**
 * return message error of exception
 * @param {*} e  object type Error
 * @returns
 */
const getErrorMessage = (e: any) => {
  if (e.hasOwnProperty("errors") && e.errors.length > 0) {
    return ` ${e.errors[0].message}`;
  }
  if (e.hasOwnProperty("parent")) {
    if (e.parent.hasOwnProperty("sqlMessage")) return ` ${e.parent.sqlMessage}`;
    return ` ${e.parent.toString()}`;
  }
  if (e.hasOwnProperty("message")) {
    return ` ${e.message}`;
  }
  return ` ${e.toString()}`;
};

/**
 * response warning
 * @param {*} res
 * @param {*} data
 * @param {*} message
 * @returns
 */
const responseWarningorError = (
  res: Response,
  data: Object,
  message: string,
  code: number
) => {
  const result = res.status(code).json({
    result: false,
    data,
    message,
  });
  return result;
};

module.exports = {
  genRandomString,
  encryptPassword,
  saltHashPassword,
  getAttributesOfModel,
  getErrorMessage,
  responseWarningorError,
};
