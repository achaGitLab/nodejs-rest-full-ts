import path from "path";
const cors = require("cors");
const errors = require("./src/components/errors");
import { Application, Request, Response } from "express";

// Import Endpoints
import { test } from "./src/api/app/http/test";
import { user } from "./src/api/app/http/user";

module.exports = (app: Application): void => {
  const whitelist = [""];

  const corsOptions = {
    origin: whitelist,
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  };

  app.use(cors(corsOptions));

  app.use('/api/tests', test); // route for helloworld
  app.use('/api/users', user); // route for helloworld

  // All undefined asset or api routes should return a 404
  app
    .route("/:url(api|auth|components|app|bower_components|assets|public)/*")
    .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route("/*").get((req: Request, res: Response) => {
    const home = `${app.get("appPath")}/index.html`;
    res.sendFile(path.resolve(home));
  });
};
